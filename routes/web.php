<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

// Route::get('home/login');
// Route::resource('home', 'loginController');

Route::post('/login', ['as' => 'login', 'uses' => 'loginController@login']); //to add controller method route 
Route::post('/welcome', ['as' => 'welcome', 'uses' => 'loginController@welcome']); //to add controller method route 

//Route::post("login", "loginController@login");
