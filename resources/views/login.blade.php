<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Login - Create</title>
</head>
<body >
	<h1>Login Page</h1>
     <div class=" col-sm-offset-4 col-lg-offset-4" style="margin-top : 100px">		
		<div class="col-md-6">				    	
			<div class="panel-body">
               	<form class="form-horizontal" action="{{ route('login') }}" method="post">
					<fieldset>
						<!-- Name input-->
                        
						<div class="form-group">
							<label class="col-md-3 control-label" for="password">Email</label>
                            
							<div class="col-md-9">
                                
								<input id="username" name="email" required type="email" placeholder="Email" class="form-control" />
                            </div>
						</div>
					
						<!-- Email input-->
						<div class="form-group">
							<label class="col-md-3 control-label" for="password">Password</label>
							<div class="col-md-9">
								<input id="password" name="password" required type="password" placeholder=" Password" class="form-control" />
                            </div>
						</div>				
						
						<!-- Form actions -->
						<div class="form-group">
							<div class="col-md-12 ">
								<button type="submit" name="login" class="btn btn-primary btn-lg col-sm-offset-6">Login</button>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>          
</body>
</html>