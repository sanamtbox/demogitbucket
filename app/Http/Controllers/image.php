<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $title
 * @property string $name
 * @property string $size
 * @property string $type
 * @property string $created_at
 * @property string $updated_at
 */
class image extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['title', 'name', 'size', 'type', 'created_at', 'updated_at'];

}
